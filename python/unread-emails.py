#!/usr/bin/env python
from random import randint
import unicornhat as UH
from time import sleep
import json

width = 8
height = 8
rows = []
row_pointer = 0
jsonText = ""

UH.brightness(0.1)

def step():
  #  UH.clear();
    update_display()


def update_display():
    try:
        file = open("../emails2.json")
    except:
        return
    jsonText = file.read();
    file.close();
    data = json.loads(jsonText)


    #loop json array
    i = 0
    for x in range(8):

        if data['rows'].has_key(str(i)):

            #print ('row: ' + str(i) + 'unread: ' + data['rows'][str(i)]['unread'])

            for n in range(data['rows'][str(i)]['value']):
                #print ('*' + str(n))
                UH.set_pixel(7 - i,n,data['rows'][str(i)]['red'],data['rows'][str(i)]['green'],data['rows'][str(i)]['blue'])

            UH.show()
            i = i + 1

while True:
    step()
    sleep(1.00)
